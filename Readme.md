Caspers Rentals API

ABOUT
This is the second attempt of this application. It is now a springboot application.
I included junits and tested line coverage using Jacoco.


OPERATIONS
There are three operations to this API.

1. /calculator/order/add
    OPTIONAL PARAMS, USED TO SPECIFY WHAT CARS ARE DESIRED TO RENT
    * type - "car", "electric", "diesel", "semi", "yacht", "cargo", "speed", "barge" DEFAULT = car
    * color - any desired color. String value representation. DEFAULT = black
    * range - any positive double value. How long user needs car. DEFAULT = 100
    * quantity - any positive integer value. How many needed to rent. DEFAULT = 1
    * certefied - boolean for those who can rent semis. DEFAULT = 1
    
    RUNNING LOCAL EXAMPLE:
    localhost:8080/calculator/order/add?type=speed&color=blue&range=50

2. /calculator/order/summary
    DISPLAYS OUTPUT OF CARS ADDED TO ORDER

3. /calculator/order/clear
    CLEARS CURRENT ORDER